/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * unityurpwebgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtWebEngine 1.8

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'unityurpwebgl.nitanmarcel'
    automaticOrientation: true
    backgroundColor : "transparent"

    Page {
        anchors.fill: parent

        WebEngineView {
            anchors.fill: parent
            id: webView
            url: "http://localhost:20989"
            settings.playbackRequiresUserGesture = false

            onLoadProgressChanged: {
                if (loadProgress === 100)
                    window.showFullScreen()
            }

            onJavaScriptDialogRequested: function (request) {
                request.accepted = true;
                var popup = PopupUtils.open(dialogComponent, this, {"title": request.title, "message": request.message})
                    popup.dialogAccepted.connect(function () {
                        request.dialogAccept()
                        PopupUtils.close(popup)
                    })
                    popup.dialogRejected.connect(function () {
                        request.dialogReject()
                        PopupUtils.close(popup)
                    })
            }
        }
    }

    Component {
        id: dialogComponent

        Dialog {

            id: dialog

            title: i18n.tr("Dialog")
            property var message

            signal dialogAccepted()
            signal dialogRejected()

            Label {
                wrapMode: Text.WordWrap
                text: jsDialog.message
            }

            Button {
                text: i18n.tr("OK")
                color: UbuntuColors.green
                onClicked: () => dialogAccepted()
            }
            Button {
                text: i18n.tr("Cancel")
                onClicked: () => dialogRejected()
            }
        }
    }
}
