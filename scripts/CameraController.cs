using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private PlayerActions cameraInput;
    private Camera mainCamera;
    void Awake()
    {
        cameraInput = new PlayerActions();
        mainCamera = Camera.main;
    }

    private void OnEnable()
    { 
        cameraInput?.Enable();
    }

    private void OnDisable()
    {
        cameraInput?.Disable();
    }

    private void Update()
    {
        Vector2 lookDelta = cameraInput.Camera.Look.ReadValue<Vector2>();
        mainCamera.transform.eulerAngles = mainCamera.transform.eulerAngles - (new Vector3(lookDelta.y, lookDelta.x * -1, 0) * 50 * Time.deltaTime);

        Vector2 moveDelta = cameraInput.Camera.Move.ReadValue<Vector2>();
        mainCamera.transform.Translate(new Vector3(moveDelta.x, 0.0f, moveDelta.y) * 3 * Time.deltaTime);
    }
}
