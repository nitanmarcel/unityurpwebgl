/*
Copyright 2022  Marcel Alexandru Nitan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QObject>
#include <QQuickView>
#include <QtQuickControls2/QQuickStyle>

#include "qhttpserver.hpp"
#include "qhttpserverconnection.hpp"
#include "qhttpserverrequest.hpp"
#include "qhttpserverresponse.hpp"

#include <QDebug>

#include <map>
#include <stdlib.h>
#include <iostream>
#include <string>

std::map<std::string, std::string> mimes;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QGuiApplication *app = new QGuiApplication(argc, (char **)argv);
    QQuickStyle::setStyle("Suru");
    app->setApplicationName("unityurpwebgl.nitanmarcel");

    mimes[".html"] = "text/html";
    mimes[".css"] = "text/css";
    mimes[".js"] = "application/javascript";
    mimes[".wasm"] = "application/wasm";
    mimes[".woff2"] = "application/font-woff2";
    mimes[".png"] = "image/png";
    mimes[".icon"] = "image/x-icon";
    mimes[".br"] = "application/javascript";

    qhttp::server::QHttpServer server;
    server.listen(QHostAddress::LocalHost, 20989,
                  [](qhttp::server::QHttpRequest *req, qhttp::server::QHttpResponse *res)
                  {
                      QString docname = "./www/" + (req->url().toString() == ("/") ? ("/index.html") : req->url().toString());
                      if (!QFile(docname).exists())
                          docname = QString("./www/index.html");
                      QFile doc(docname);
                      doc.open(QFile::ReadOnly);

                      res->addHeader("Content-Length", QString::number(doc.size()).toUtf8());
                      res->addHeader("Connection", "keep-alive");
                      auto doc_str = docname.toStdString();
                      auto doc_ext = doc_str.substr(doc_str.find_last_of('.'));
                      if (doc_ext == ".br")
                      {
                          res->addHeader("Content-Encoding", "br");
                          doc_str.erase(doc_str.find(".br"), sizeof(".br"));
                          doc_ext = doc_str.substr(doc_str.find_last_of('.'));
                      }
                      else if (doc_ext == ".gz")
                      {
                          res->addHeader("Content-Encoding", "gzip");
                          doc_str.erase(doc_str.find(".gz"), sizeof(".gz"));
                          doc_ext = doc_str.substr(doc_str.find_last_of('.'));
                      }
                      std::cout << doc_ext << std::endl;
                      if (mimes.count(doc_ext) > 0)
                          res->addHeader("Content-Type", mimes[doc_ext].data());
                      else
                          res->addHeader("Content-Type", "application/octet-stream");
                      res->setStatusCode(qhttp::TStatusCode::ESTATUS_OK);
                      res->write(doc.readAll());
                  });

    QQuickView *view = new QQuickView();
    view->setSource(QUrl("qrc:/Main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();
    return app->exec();
}
